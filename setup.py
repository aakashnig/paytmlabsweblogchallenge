#!/usr/bin/env python
from distutils.core import setup
from setuptools import find_packages

setup(name='PayTMLabsChallenge',
      version='0.0.1',
      description='PayTMLabChallenge',
      author='Aakash nigam',
      author_email='nigam.akaash@gmail.com',
      packages=find_packages(exclude=['*.tests']),
      install_requires=['py4j==0.9.0'])