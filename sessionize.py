"""Sessionize IP log"""
from __future__ import print_function
from pyspark import SparkConf, SparkContext
import socket
import sys
from socket import inet_aton
import struct
from operator import add
from pyspark import SparkContext
import unicodedata
from pyspark import *
from pyspark import SparkFiles
import dateutil.parser as dp

APP_NAME = "PayTMLabChallenge"

class TimeStampValue(object):
  def __init__(self, timestamp=None, value=None):
      self.timestamp = dp.parse(timestamp).strftime('%s')
      self.value = value

class Sessions:
  def __init__(self,session,list):
    self.sesion=session
    self.list= ipaddress

#ForSorting IP Addresses
def split_ip(ip):
    return tuple(int(part) for part in ip.split('.'))

def my_key(item):
    return split_ip(item[0])

#Identity function for FlatMapValues
def f(x): return x

#Function to figure out distinct visits by sessions. 
def func_distinct_visits_per_session(x):
    rdd = spark.parallelize(list(x)) \
                .distinct() \

def function_sessionize(x):
  count=0
  TimeStampValueList = []
  result=()
  for itr in x:
    ll = itr.split(' ** ')
    ll[0].strip()
    ll[1].strip()
    TimeStampValueList.append(TimeStampValue(str(ll[0]),str(ll[1])))
  length=len(TimeStampValueList)
  for i in range(0,length-1):
    TimeStampValueList[i].timeStamp=int(TimeStampValueList[i].timestamp)-int(TimeStampValueList[0].timestamp)
  #for i in range(0,length-1):
  #  listofips = []
  #  for j in range(0,int(TimeStampValueList[length-1].timestamp),900):
  #    while(int(TimeStampValueList[i].timestamp<j)):
  #     listofips.append(TimeStampValueList[i].value)
  #  result = tuple(listofips)
  return result

def main(sc,filename):
    IpRDD = sc.textFile(filename).map(lambda x: x.split(' ')) \
                                 .filter(lambda x : len(x)>15) \
                                 .map(lambda x: (x[2].split(':')[0] ,x[0] ,x[12])) \
                                 .map(lambda x: (x[0].encode('utf-8') , x[1].encode('utf-8') +' ** ' + x[2].encode('utf-8') )) \
                                 .groupByKey() \
                                 .sortByKey(my_key) \
                                 .mapValues(list) \
                                 .mapValues(function_sessionize)\
                                 .saveAsTextFile('./sessionize_data/sessions.txt')

if __name__ == "__main__":
    # Configure OPTIONS
    conf = SparkConf().setAppName(APP_NAME  )
    conf = conf.setMaster("local[*]")
    #in cluster this will be like "spark://ec2-0-17-03-078.compute-#1.amazonaws.com:7077"
    sc   = SparkContext(conf=conf)
    filename = sys.argv[1]
    main(sc, filename)